#pragma once
#include "Ventana.h"
#include "Paletas.h"
#include "TeclasUsables.h"
#include <time.h>

enum class powerUpPunto
{
    Ninguno,
    Escudo,
    AgrandarPaleta,
    MasVelocidad,
    MenosVelocidad
};

enum class powerUpTiempo
{
    Ninguno,
    Multiball,
    Obtaculos,
    DireccionInvertida,
    ControlesInvertidos
};

const int anchoBordes = 0;
const int puntajeMax = 10;
const int diferenciaTamanosPaleta = 50;

const Color colorBorde = WHITE;
const Color colorTope = WHITE;

struct Jugador
{
    Rectangle paleta;
    Color colorPaleta;
    int velocidadPaleta;
    int puntuacion=0;
    powerUpTiempo poderTiempo = powerUpTiempo::Ninguno;
    powerUpPunto poderPunto = powerUpPunto::AgrandarPaleta;
    bool paletaAgrandada = false;
};

struct Pelota
{
    Vector2 posicion;
    int radio = 10;
    int velocidadX = 5;
    int velocidadY = 5;
    bool colisionPaleta=false;
    bool enMovimiento = false;
};

struct ParametrosCancha {

    Rectangle bordesSupCancha;
    Rectangle bordesInfCancha;
    Rectangle bordesDerCancha;
    Rectangle bordesIzqCancha;
    Color colorCancha{ 230, 41, 55, 255 };

};

void MecanicaDeJuegoJvsJ();
