#include "controles.h"

namespace controles 
{
	teclasControles cJ1;
	teclasControles cJ2;

	void initControles()
	{
		controles::cJ1.subir = KEY_UP;
		controles::cJ1.bajar = KEY_DOWN;

		controles::cJ2.subir = KEY_W;
		controles::cJ2.bajar = KEY_S;
	}

	bool chequeoControles(direccionales teclaDirecional, int auxIngreso)
	{
		switch (teclaDirecional)
		{
		case direccionales::subirJ1:
			if (auxIngreso != controles::cJ1.bajar && auxIngreso != controles::cJ2.bajar && auxIngreso != controles::cJ2.subir)
			{
				return true;
			}

			break;
		case direccionales::bajarJ1:
			if (auxIngreso != controles::cJ1.subir && auxIngreso != controles::cJ2.bajar && auxIngreso != controles::cJ2.subir)
			{
				return true;
			}
			break;
		case direccionales::subirJ2:
			if (auxIngreso != controles::cJ1.subir && auxIngreso != controles::cJ2.bajar && auxIngreso != controles::cJ1.subir)
			{
				return true;
			}
			break;
		case direccionales::bajarJ2:
			if (auxIngreso != controles::cJ1.subir && auxIngreso != controles::cJ1.bajar && auxIngreso != controles::cJ2.subir)
			{
				return true;
			}
			break;
		default:

			break;
		}
		return false;

	}

	int setearTeclaControles(int valorAnterior, direccionales teclaDirecional)
	{
		int auxIngreso = 0;
		int valorDefault = valorAnterior;
		auxIngreso = GetKeyPressed();

		if (auxIngreso != 0)
		{
			if (auxIngreso > 96 && auxIngreso < 123)
			{
				auxIngreso -= 32;
			}
			if ((auxIngreso > 64 && auxIngreso < 91) || (auxIngreso > 261 && auxIngreso < 266) && auxIngreso != 0)
			{
				if (chequeoControles(teclaDirecional, auxIngreso))
				{
					return auxIngreso;
				}

			}
		}
		else
		{
			if (IsKeyPressed(KEY_UP))
			{
				auxIngreso = KEY_UP;
			}
			else
			{
				if (IsKeyPressed(KEY_DOWN))
				{
					auxIngreso = KEY_DOWN;
				}
				else
				{
					if (IsKeyPressed(KEY_LEFT))
					{
						auxIngreso = KEY_LEFT;
					}
					else
					{
						if (IsKeyPressed(KEY_RIGHT))
						{
							auxIngreso = KEY_RIGHT;
						}
					}
				}
			}

			if (chequeoControles(teclaDirecional, auxIngreso) && auxIngreso != 0)
			{
				return auxIngreso;
			}

		}


		return valorDefault;

	}
};








