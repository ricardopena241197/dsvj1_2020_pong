#ifndef CONTROLES_H
#include "raylib.h"
#define CONTROLES_H
namespace controles
{
	struct teclasControles {

		int subir;
		int bajar;
	};

	extern teclasControles cJ1;
	extern teclasControles cJ2;	

	enum class direccionales { subirJ1, bajarJ1, subirJ2, bajarJ2 };

	void initControles();
	int setearTeclaControles(int valorAnterior, direccionales teclaDirecional);
}

#endif // !CONTROLES_H






