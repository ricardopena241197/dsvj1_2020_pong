#include "paletas.h"

namespace confPaletas
{
	paletas pJ1;
	paletas pJ2;

    void setearColorPaleta(identificadorColor idColor, Color& color)
    {
        switch (idColor)
        {
        case identificadorColor::Negro:
            color = BLACK;
            break;
        case identificadorColor::Rojo:
            color = RED;
            break;
        case identificadorColor::Violeta:
            color = VIOLET;
            break;
        case identificadorColor::Azul:
            color = DARKBLUE;
            break;
        case identificadorColor::Celeste:
            color = SKYBLUE;
            break;
        default:
            break;
        }
    }


    void initPaletas()
    {
        confPaletas::pJ1.tamano = tamanos::Mediano;
        confPaletas::pJ1.idColor = identificadorColor::Negro;
        setearColorPaleta(confPaletas::pJ1.idColor, confPaletas::pJ1.color);

        confPaletas::pJ2.tamano = tamanos::Mediano;
        confPaletas::pJ2.idColor = identificadorColor::Negro;
        setearColorPaleta(confPaletas::pJ2.idColor, confPaletas::pJ2.color);
    }

    int setearTamanoPaleta(tamanos tamano) {

        switch (tamano)
        {
        case tamanos::Pequeno:

            return altoPeneno;

            break;
        case tamanos::Mediano:

            return altoMediano;
            break;
        case tamanos::Grande:

            return altoGrande;
            break;
        default:
            break;
        }
    }
    int setearTamanoPaleta(tamanos tamano, int& velocidad) {

        switch (tamano)
        {
        case tamanos::Pequeno:
            velocidad = velocidadPeneno;
            return altoPeneno;

            break;
        case tamanos::Mediano:
            velocidad = velocidadMediano;
            return altoMediano;
            break;
        case tamanos::Grande:
            velocidad = velocidadGrande;
            return altoGrande;
            break;
        default:
            break;
        }
    }

    void setearPaletaMostrada(Rectangle& paletaMostrada, tamanos tamano)
    {
        paletaMostrada.height = setearTamanoPaleta(tamano);
        paletaMostrada.width = anchoTodasPaletas;
        paletaMostrada.x = ((iAnchoVentana / 4) * 3) - paletaMostrada.width / 2;
        paletaMostrada.y = iAltoVentana / 2 - paletaMostrada.height / 2;
    }

}


