
#ifndef PALETAS_H
#define PALETAS_H
#include "raylib.h"
#include "Ventana/ventana.h"
using namespace ventana;

namespace confPaletas
{
	enum class tamanos { Pequeno, Mediano, Grande };

	const int velocidadPeneno = 20;
	const int velocidadMediano = 10;
	const int velocidadGrande = 5;

	const int altoPeneno = 50;
	const int altoMediano = 100;
	const int altoGrande = 150;
	const int anchoTodasPaletas = 10;

	const int iCantTamanos = 3;
	const int iCantColores = 5;

	enum class identificadorColor { Negro, Rojo, Violeta, Azul, Celeste };


	struct paletas
	{
		Color color;
		identificadorColor idColor;
		tamanos tamano;
	};

	extern paletas pJ1;
	extern paletas pJ2;

	void initPaletas();
	void setearPaletaMostrada(Rectangle& paletaMostrada, tamanos tamano);
	void setearColorPaleta(identificadorColor idColor, Color& color);
	int setearTamanoPaleta(tamanos tamano);
	int setearTamanoPaleta(tamanos tamano, int& velocidad);
}

#endif // !PALETAS_H

