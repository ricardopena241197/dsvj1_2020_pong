#include "chequeos.h"


using namespace jugar;

namespace chequeos
{
    bool ChequeoVictoria(Jugador jugador1, Jugador jugador2)
    {
        return (jugador1.puntuacion == puntajeMax || jugador2.puntuacion == puntajeMax);
    }

    bool ChequeoColisionBordesSup(Pelota pelota)
    {
        return(pelota.posicion.y - pelota.radio <= 0 || pelota.posicion.y + pelota.radio >= iAltoVentana);

    }

    bool ChequeoColisionBordesInf(Pelota pelota)
    {
        return(pelota.posicion.y - pelota.radio <= 0 || pelota.posicion.y + pelota.radio >= iAltoVentana);

    }

    bool ChequeoColisionBordesDer(Pelota pelota)
    {
        return(pelota.posicion.x + pelota.radio >= iAnchoVentana);
    }

    bool ChequeoColisionBordesIzq(Pelota pelota)
    {
        return(pelota.posicion.x - pelota.radio <= 0);
    }

    bool ChequeoGenerarPoderTiempo(clock_t& tiempoAnterior)
    {
        int tiempoActual = clock();
        const int entreTiempo = 30000;
        if (tiempoActual - tiempoAnterior >= entreTiempo)
        {
            tiempoAnterior = tiempoActual;
            return true;
        }
        else
        {
            return false;
        }
    }

    bool ChequeoGenerarPoderPunto(clock_t& tiempoAnterior)
    {
        int tiempoActual = clock();
        const int entreTiempo = 5000;
        if (tiempoActual - tiempoAnterior >= entreTiempo)
        {
            tiempoAnterior = tiempoActual;
            return true;
        }
        else
        {
            return false;
        }
    }

    bool ChequeoColisionSupInfObjeto(Rectangle obstaculo, Pelota pelota)
    {
        if (CheckCollisionCircleRec(pelota.posicion, pelota.radio, obstaculo))
        {
            if (pelota.posicion.x > obstaculo.x - pelota.radio && pelota.posicion.x < obstaculo.x + tamanoObstaculo + pelota.radio)
            {
                return true;
            }
        }
        return false;
    }

    bool ChequeoColisionDerIzqObjeto(Rectangle obstaculo, Pelota pelota)
    {
        if (CheckCollisionCircleRec(pelota.posicion, pelota.radio, obstaculo))
        {
            if (pelota.posicion.y > obstaculo.y - pelota.radio && pelota.posicion.y < obstaculo.y + tamanoObstaculo + pelota.radio)
            {
                return true;
            }
        }
        return false;
    }

    bool ChequeoTerminadoMultiBall(Pelota pelotas[cantidaPelotas])
    {
        for (short i = 0; i < cantidaPelotas; i++)
        {
            if (pelotas[i].enMovimiento)
            {
                return true;
            }

        }
        return false;
    }
}
