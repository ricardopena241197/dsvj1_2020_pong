#ifndef CHEQUEOS_H
#define CHEQUEOS_H

#include "raylib.h"
#include "Gameplay/MecanicasDeJuego/jugar.h"
#include <time.h>

namespace chequeos
{
	bool ChequeoVictoria(jugar::Jugador jugador1, jugar::Jugador jugador2);

	bool ChequeoColisionBordesSup(jugar::Pelota pelota);

	bool ChequeoColisionBordesInf(jugar::Pelota pelota);

	bool ChequeoColisionBordesDer(jugar::Pelota pelota);

	bool ChequeoColisionBordesIzq(jugar::Pelota pelota);

	bool ChequeoGenerarPoderTiempo(clock_t& tiempoAnterior);

	bool ChequeoGenerarPoderPunto(clock_t& tiempoAnterior);

	bool ChequeoTerminadoMultiBall(jugar::Pelota pelotas[jugar::cantidaPelotas]);

	bool ChequeoColisionSupInfObjeto(Rectangle obstaculo, jugar::Pelota pelota);

	bool ChequeoColisionDerIzqObjeto(Rectangle obstaculo, jugar::Pelota pelota);
}


#endif
