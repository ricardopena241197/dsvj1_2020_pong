#include "dibujar.h"

namespace dibujar
{
	void DibujarCancha(ParametrosCancha cancha)
	{
		DrawRectangleRec(cancha.bordesSupCancha, colorTope);
		DrawRectangleRec(cancha.bordesInfCancha, colorBorde);
		DrawRectangleRec(cancha.bordesDerCancha, colorBorde);
		DrawRectangleRec(cancha.bordesIzqCancha, colorBorde);
	}

	void DibujarPuntaje(Jugador jugador1, Jugador jugador2, int ronda)
	{
		const int tamTitulo = 30;
		const int cantLetrasTitulo = 7;
		const int tamPuntajes = 25;
		Vector2 posTitulo;
		Vector2 posPuntaje1;
		Vector2 posPuntaje2;

		Color colorPuntaje = DARKBLUE;

		posTitulo.x = (iAnchoVentana / 2) - ((tamTitulo * (cantLetrasTitulo / 2 + 1)) / 2);
		posTitulo.y = 0;
		posPuntaje1.x = iAnchoVentana / 8;
		posPuntaje1.y = 0;
		posPuntaje2.x = ((iAnchoVentana / 8) * 6) - tamPuntajes * 2;
		posPuntaje2.y = 0;

		DrawText(TextFormat("Ronda: %i", ronda), posTitulo.x, posTitulo.y, tamTitulo, colorPuntaje);

		DrawText(TextFormat("Jugador 2: %i", jugador2.puntuacion), posPuntaje1.x, posPuntaje1.y, tamPuntajes, colorPuntaje);

		DrawText(TextFormat("Jugador 1: %i", jugador1.puntuacion), posPuntaje2.x, posPuntaje2.y, tamPuntajes, colorPuntaje);
	}

	void DibujarMensajeGanador(int jugadorGanador)
	{
		const int tamMensaje = 50;
		const int cantLetrasMensaje = 18;
		const int cantLetrasInstruccion = 32;
		const int tamInstruccion = 20;
		Vector2 posMensaje;
		Vector2 posInstruccion;
		Color colorMensaje = DARKBLUE;
		posMensaje.x = (iAnchoVentana / 2) - ((tamMensaje * (cantLetrasMensaje / 2 + 1)) / 2);
		posMensaje.y = iAltoVentana / 2;
		posInstruccion.x = (iAnchoVentana / 2) - ((tamInstruccion * (cantLetrasInstruccion / 2 + 1)) / 2) - 50;;
		posInstruccion.y = posMensaje.y + 100;

		DrawText(TextFormat("Ganador: Jugador %i", jugadorGanador), posMensaje.x, posMensaje.y, tamMensaje, colorMensaje);

		DrawText("PRECIONA ESPACIO PARA CONTINUAR", posInstruccion.x, posInstruccion.y, tamInstruccion, colorMensaje);
	}
}
