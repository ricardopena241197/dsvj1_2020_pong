#ifndef DIBUJAR_H
#include "Gameplay/MecanicasDeJuego/jugar.h"
#define DIBUJAR_H
using namespace jugar;

namespace dibujar
{
	void DibujarCancha(ParametrosCancha cancha);
	void DibujarPuntaje(Jugador jugador1, Jugador jugador2, int ronda);
	void DibujarMensajeGanador(int jugadorGanador);
}
#endif // !DIBUJAR_H
