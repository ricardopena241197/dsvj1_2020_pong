#include "jugar.h"
#include "Gameplay/Chequeos/chequeos.h"
#include "Gameplay/Dibujar/dibujar.h"


using namespace chequeos;
using namespace dibujar;


namespace jugar
{
    void reseteoPelota(Pelota& pelota);
    void init();
    void imput(Jugador& jugador1, Jugador& jugador2, ParametrosCancha cancha, bool& pelotaEnMovimiento, bool& auxSalida, bool IAActiva, Pelota pelota);
    void update(bool IAActiva);
   
    void deinit();
    void colisionesPelota(Pelota& pelota, Jugador& jugador1, Jugador& jugador2, int& ronda);
    void seteoMultiball(Pelota  pelotas[cantidaPelotas]);
    void seteoObstaculos(Rectangle& obstaculo1, Rectangle& obstaculo2);
    void colisionPelotaObtaculos(Jugador jugador1, Jugador jugador2, Pelota& pelota);

    powerUpTiempo generarPoderTiempoRandom();
    powerUpPunto generarPoderPuntoRandom();

    bool juegoInicializado= false;
    bool auxGanar;
    bool auxSalida;
    clock_t tiempoAnterior1;
    clock_t tiempoAnterior2;
    Jugador jugador1;
    Jugador jugador2;    
    Pelota pelotas[cantidaPelotas];
    ParametrosCancha cancha;
    int ronda = 0;
    int jugadorGanador = 0;

    void mecanicaDeJuegoJvsJ(bool IAActiva)
    {  
        if (!juegoInicializado)
        {
            init();
            juegoInicializado = true;
        }        
        update(IAActiva);
        auxGanar = ChequeoVictoria(jugador1, jugador2);       

        if (auxGanar)
        {
            ClearBackground(RAYWHITE);
            DibujarCancha(cancha);
            DibujarPuntaje(jugador1, jugador2, ronda);
            jugadorGanador = (jugador1.puntuacion > jugador2.puntuacion) ? 1 : 2;
            DibujarMensajeGanador(jugadorGanador);
            if (IsKeyPressed(KEY_SPACE))
            {
                juegoInicializado = false;
                pong::juego::escenaActual = Escenas::Jugar;
            }
        }
    }

    void reseteoPelota(Pelota& pelota)
    {
        pelota.posicion.x = iAnchoVentana / 2;
        pelota.posicion.y = iAltoVentana / 2;
        pelota.enMovimiento = false;
    }

    void aumentoTamaņoPaleta(Jugador& jugador)
    {
        int tamanoAumentado = (int)confPaletas::pJ1.tamano + 1;
        
        jugador.paleta.height = (confPaletas::pJ1.tamano == confPaletas::tamanos::Grande) ? jugador.paleta.height : setearTamanoPaleta((confPaletas::tamanos)tamanoAumentado, jugador.velocidadPaleta);
        if (jugador.paleta.y >= iAltoVentana - jugador.paleta.height)
        {
            jugador.paleta.y -= diferenciaTamanosPaleta;
        }
        else
        {
            if (jugador.paleta.y >= diferenciaTamanosPaleta / 2)
            {
                jugador.paleta.y -= diferenciaTamanosPaleta / 2;
            }

        }


    }

    void disminuirTamaņoPaleta(Jugador& jugador)
    {
        jugador.paleta.height = setearTamanoPaleta(confPaletas::pJ1.tamano, jugador.velocidadPaleta);
        jugador.paleta.y += diferenciaTamanosPaleta / 2;
    }

    void init()
    {
        auxGanar = false;
        auxSalida = false;
        tiempoAnterior1 = clock();
        tiempoAnterior2 = clock();
             
        jugadorGanador = 0;
        
        ronda = 0;

        for (short i = 0; i < cantidaPelotas; i++)
        {
            reseteoPelota(pelotas[i]);
        }

        jugador1.paleta.height = setearTamanoPaleta(confPaletas::pJ1.tamano, jugador1.velocidadPaleta);
        jugador1.colorPaleta = confPaletas::pJ1.color;
        jugador1.paleta.width = confPaletas::anchoTodasPaletas;
        jugador1.paleta.x = iAnchoVentana - anchoBordes - 25 - jugador1.paleta.width / 2;
        jugador1.paleta.y = iAltoVentana / 2 - jugador1.paleta.height / 2;

        jugador2.paleta.height = setearTamanoPaleta(confPaletas::pJ2.tamano, jugador2.velocidadPaleta);
        jugador2.colorPaleta = confPaletas::pJ2.color;
        jugador2.paleta.width = confPaletas::anchoTodasPaletas;
        jugador2.paleta.x = 25 + anchoBordes - jugador2.paleta.width / 2;
        jugador2.paleta.y = iAltoVentana / 2 - jugador2.paleta.height / 2;

        cancha.bordesSupCancha.height = anchoBordes;
        cancha.bordesSupCancha.width = iAnchoVentana;
        cancha.bordesSupCancha.x = 0;
        cancha.bordesSupCancha.y = 0;

        cancha.bordesIzqCancha.height = iAltoVentana;
        cancha.bordesIzqCancha.width = anchoBordes;
        cancha.bordesIzqCancha.x = 0;
        cancha.bordesIzqCancha.y = 0;

        cancha.bordesDerCancha.height = iAltoVentana;
        cancha.bordesDerCancha.width = anchoBordes;
        cancha.bordesDerCancha.x = iAnchoVentana - anchoBordes;
        cancha.bordesDerCancha.y = 0;

        cancha.bordesInfCancha.height = anchoBordes;
        cancha.bordesInfCancha.width = iAnchoVentana;
        cancha.bordesInfCancha.x = 0;
        cancha.bordesInfCancha.y = iAltoVentana - anchoBordes;
    }

    void imput(Jugador& jugador1, Jugador& jugador2, ParametrosCancha cancha, bool& pelotaEnMovimiento, bool& auxSalida, bool IAActiva, Pelota pelota)
    {
        if (IsKeyDown(controles::cJ1.subir))
        {
            if (jugador1.poderTiempo == powerUpTiempo::ControlesInvertidos)
            {
                if (!CheckCollisionRecs(cancha.bordesInfCancha, jugador1.paleta))
                {
                    jugador1.paleta.y += jugador1.velocidadPaleta;
                }
            }
            else
            {
                if (!CheckCollisionRecs(cancha.bordesSupCancha, jugador1.paleta))
                {
                    jugador1.paleta.y -= jugador1.velocidadPaleta;
                }
            }
        }

        if (IsKeyDown(controles::cJ1.bajar))
        {
            if (jugador1.poderTiempo == powerUpTiempo::ControlesInvertidos)
            {
                if (!CheckCollisionRecs(cancha.bordesSupCancha, jugador1.paleta))
                {
                    jugador1.paleta.y -= jugador1.velocidadPaleta;
                }
            }
            else
            {
                if (!CheckCollisionRecs(cancha.bordesInfCancha, jugador1.paleta))
                {
                    jugador1.paleta.y += jugador1.velocidadPaleta;
                }
            }

        }

        if (!IAActiva)
        {
            if (IsKeyDown(controles::cJ2.subir))
            {
                if (jugador2.poderTiempo == powerUpTiempo::ControlesInvertidos)
                {
                    if (!CheckCollisionRecs(cancha.bordesInfCancha, jugador2.paleta))
                    {
                        jugador2.paleta.y += jugador2.velocidadPaleta;
                    }
                }
                else
                {
                    if (!CheckCollisionRecs(cancha.bordesSupCancha, jugador2.paleta))
                    {
                        jugador2.paleta.y -= jugador2.velocidadPaleta;
                    }
                }
            }

            if (IsKeyDown(controles::cJ2.bajar))
            {
                if (jugador2.poderTiempo == powerUpTiempo::ControlesInvertidos)
                {
                    if (!CheckCollisionRecs(cancha.bordesSupCancha, jugador2.paleta))
                    {
                        jugador2.paleta.y -= jugador2.velocidadPaleta;
                    }
                }
                else
                {
                    if (!CheckCollisionRecs(cancha.bordesInfCancha, jugador2.paleta))
                    {
                        jugador2.paleta.y += jugador2.velocidadPaleta;
                    }
                }
            }
        }
        else
        {
            if (pelota.posicion.y < jugador2.paleta.y + jugador2.paleta.height / 2 - toleranciaIA)
            {
                if (jugador2.poderTiempo == powerUpTiempo::ControlesInvertidos)
                {
                    if (!CheckCollisionRecs(cancha.bordesInfCancha, jugador2.paleta))
                    {
                        jugador2.paleta.y += jugador2.velocidadPaleta;
                    }
                }
                else
                {
                    if (!CheckCollisionRecs(cancha.bordesSupCancha, jugador2.paleta))
                    {
                        jugador2.paleta.y -= jugador2.velocidadPaleta;
                    }
                }
            }

            if (pelota.posicion.y > jugador2.paleta.y + jugador2.paleta.height / 2 + toleranciaIA)
            {
                if (jugador2.poderTiempo == powerUpTiempo::ControlesInvertidos)
                {
                    if (!CheckCollisionRecs(cancha.bordesSupCancha, jugador2.paleta))
                    {
                        jugador2.paleta.y -= jugador2.velocidadPaleta;
                    }
                }
                else
                {
                    if (!CheckCollisionRecs(cancha.bordesInfCancha, jugador2.paleta))
                    {
                        jugador2.paleta.y += jugador2.velocidadPaleta;
                    }
                }
            }
        }

        if (IsKeyPressed(teclas_Usables::inicioMovimientoPelota) && !pelotaEnMovimiento)
        {
            if (jugador1.poderTiempo != powerUpTiempo::Multiball)
            {
                pelotaEnMovimiento = true;
            }

        }

        if (IsKeyPressed(teclas_Usables::menuPausa))
        {
            pong::juego::escenaActual = Escenas::Pausa;
            if (IAActiva)
            {
                pong::juego::escenaAnterior = Escenas::JvsIA;
            }
            else
            {
                pong::juego::escenaAnterior = Escenas::JvsJ;
            }
        }

    }

    void update(bool IAActiva)
    {
        const int duracionPowerUpTiempo = 10000;  //Milisegundos

        powerUpPunto poderRandomPunto = powerUpPunto::Ninguno;

        int cantPelotas = 0;
        bool activarMultiball = false;
        bool activarObjetos = false;

        imput(jugador1, jugador2, cancha, pelotas[0].enMovimiento, auxSalida, IAActiva, pelotas[0]);

        if (!pelotas[0].enMovimiento)
        {
            tiempoAnterior1 = clock();
            tiempoAnterior2 = clock();
        }

        if (ChequeoGenerarPoderTiempo(tiempoAnterior1) && jugador1.poderTiempo == powerUpTiempo::Ninguno)
        {
            jugador1.poderTiempo = generarPoderTiempoRandom();

            switch (jugador1.poderTiempo)
            {
            case powerUpTiempo::Multiball:
                activarMultiball = true;
                break;
            case powerUpTiempo::Obstaculos:
                activarObjetos = true;
                break;

            default:
                break;
            }
        }

        if (ChequeoGenerarPoderPunto(tiempoAnterior2))
        {
            poderRandomPunto = generarPoderPuntoRandom();
            if (rand() % cantidadJugadores + 1 == 1)
            {
                if (jugador1.poderPunto == powerUpPunto::Ninguno)
                {
                    jugador1.poderPunto = poderRandomPunto;
                    if (jugador1.poderPunto == powerUpPunto::MasVelocidad || jugador1.poderPunto == powerUpPunto::MenosVelocidad)
                    {
                        jugador1.poderPunto = jugador2.poderPunto;
                        pelotas[0].velocidadAlterada = true;
                    }
                }
            }
            else
            {
                if (jugador2.poderPunto == powerUpPunto::Ninguno)
                {
                    jugador2.poderPunto = poderRandomPunto;
                    if (jugador2.poderPunto == powerUpPunto::MasVelocidad || jugador2.poderPunto == powerUpPunto::MenosVelocidad)
                    {
                        jugador1.poderPunto = jugador2.poderPunto;
                        pelotas[0].velocidadAlterada = true;
                    }
                }
            }
        }

        if (clock() - tiempoAnterior1 >= duracionPowerUpTiempo && jugador1.poderTiempo != powerUpTiempo::Ninguno)
        {
            tiempoAnterior1 = clock();
            jugador1.poderTiempo = powerUpTiempo::Ninguno;

        }

        if (activarObjetos)
        {
            jugador1.poderTiempo = powerUpTiempo::Obstaculos;
            seteoObstaculos(jugador1.obstaculo, jugador2.obstaculo);
            activarObjetos = false;
        }

        if (activarMultiball)
        {
            jugador1.poderTiempo = powerUpTiempo::Multiball;
            seteoMultiball(pelotas);
            activarMultiball = false;
        }

        if (!ChequeoTerminadoMultiBall(pelotas) && jugador1.poderTiempo == powerUpTiempo::Multiball)
        {
            jugador1.poderTiempo = powerUpTiempo::Ninguno;
        }

        if (jugador1.poderPunto == powerUpPunto::AgrandarPaleta && !jugador1.paletaAgrandada)
        {
            aumentoTamaņoPaleta(jugador1);
            jugador1.paletaAgrandada = !jugador1.paletaAgrandada;
        }

        if (jugador2.poderPunto == powerUpPunto::AgrandarPaleta && !jugador2.paletaAgrandada)
        {
            aumentoTamaņoPaleta(jugador2);
            jugador2.paletaAgrandada = !jugador2.paletaAgrandada;
        }

        if (jugador1.poderTiempo == powerUpTiempo::Multiball)
        {
            cantPelotas = cantidaPelotas;
        }
        else
        {
            cantPelotas = 1;
        }

        for (short i = 0; i < cantidaPelotas; i++)
        {
            colisionesPelota(pelotas[i], jugador1, jugador2, ronda);

            if (pelotas[i].enMovimiento)
            {
                if (pelotas[i].velocidadAlterada)
                {
                    pelotas[i].velocidadAlterada = !pelotas[i].velocidadAlterada;
                    switch (jugador1.poderPunto)
                    {
                    case powerUpPunto::MasVelocidad:
                        pelotas[i].velocidadX = (pelotas[i].velocidadX > 0) ? velocidadEstandar + cambioVelocidad : (velocidadEstandar + cambioVelocidad) * -1;
                        pelotas[i].velocidadY = (pelotas[i].velocidadY > 0) ? velocidadEstandar + cambioVelocidad : (velocidadEstandar + cambioVelocidad) * -1;
                        break;
                    case powerUpPunto::MenosVelocidad:
                        pelotas[i].velocidadX = (pelotas[i].velocidadX > 0) ? velocidadEstandar - cambioVelocidad : (velocidadEstandar - cambioVelocidad) * -1;
                        pelotas[i].velocidadY = (pelotas[i].velocidadY > 0) ? velocidadEstandar - cambioVelocidad : (velocidadEstandar - cambioVelocidad) * -1;
                        break;

                    default:
                        break;
                    }
                }
                else
                {
                    if (jugador1.poderPunto == powerUpPunto::Ninguno)
                    {
                        pelotas[i].velocidadX = (pelotas[i].velocidadX > 0) ? velocidadEstandar : velocidadEstandar * -1;
                        pelotas[i].velocidadY = (pelotas[i].velocidadY > 0) ? velocidadEstandar : velocidadEstandar * -1;
                    }

                }
                pelotas[i].posicion.x += pelotas[i].velocidadX;
                pelotas[i].posicion.y += pelotas[i].velocidadY;
            }
        }
    }

    void draw()
    {
       
        ClearBackground(RAYWHITE);
        DrawRectangleRec(jugador1.paleta, jugador1.colorPaleta);
        DrawRectangleRec(jugador2.paleta, jugador2.colorPaleta);

        if (jugador1.poderTiempo == powerUpTiempo::Multiball)
        {
            for (short i = 0; i < cantidaPelotas; i++)
            {
                if (pelotas[i].enMovimiento)
                {
                    DrawCircleV(pelotas[i].posicion, pelotas[i].radio, GREEN);
                }
            }
        }
        else
        {
            DrawCircleV(pelotas[0].posicion, pelotas[0].radio, GREEN);
        }

        if (jugador1.poderTiempo == powerUpTiempo::Obstaculos)
        {
            DrawRectangleRec(jugador1.obstaculo, jugador1.colorObstaculo);
            DrawRectangleRec(jugador2.obstaculo, jugador2.colorObstaculo);
        }

        switch (jugador1.poderTiempo)
        {
        case powerUpTiempo::Multiball:
            DrawText("Multiball Activo", 2, iAltoVentana - 30, 25, poderTiempo);
            break;
        case powerUpTiempo::Obstaculos:
            DrawText("Obstaculos Activo", 2, iAltoVentana - 30, 25, poderTiempo);
            break;
        case powerUpTiempo::DireccionInvertida:
            DrawText("Direccion Invertida Activo", 2, iAltoVentana - 30, 25, poderTiempo);
            break;
        case powerUpTiempo::ControlesInvertidos:
            DrawText("Controles Invertidos", 2, iAltoVentana - 30, 25, poderTiempo);
            break;
        default:
            break;
        }

        if (jugador1.poderPunto != powerUpPunto::Ninguno && jugador2.poderPunto != powerUpPunto::Ninguno && jugador2.poderPunto != powerUpPunto::MasVelocidad && jugador2.poderPunto != powerUpPunto::MenosVelocidad)
        {
            DrawText("Ambos Jugadores Potenciados", (iAnchoVentana / 8) * 4, iAltoVentana - 30, 25, poderPunto);
        }
        else
        {
            if (jugador1.poderPunto != powerUpPunto::Ninguno)
            {
                switch (jugador1.poderPunto)
                {
                case powerUpPunto::Escudo:
                    DrawText("Jugador 1: Escudo", (iAnchoVentana / 8) * 4, iAltoVentana - 30, 25, poderPunto);
                    break;
                case powerUpPunto::AgrandarPaleta:
                    DrawText("Jugador 1: Paleta Agandada", (iAnchoVentana / 8) * 4, iAltoVentana - 30, 25, poderPunto);
                    break;
                case powerUpPunto::MasVelocidad:
                    DrawText("Pelota con mas Velocidad", (iAnchoVentana / 8) * 4, iAltoVentana - 30, 25, poderPunto);
                    break;
                case powerUpPunto::MenosVelocidad:
                    DrawText("Pelota con menos Velocidad", (iAnchoVentana / 8) * 4, iAltoVentana - 30, 25, poderPunto);
                    break;
                default:
                    break;
                }
            }
            else
            {
                if (jugador2.poderPunto != powerUpPunto::Ninguno)
                {
                    switch (jugador2.poderPunto)
                    {
                    case powerUpPunto::Escudo:
                        DrawText("Jugador 2: Escudo", (iAnchoVentana / 8) * 4, iAltoVentana - 30, 25, poderPunto);
                        break;
                    case powerUpPunto::AgrandarPaleta:
                        DrawText("Jugador 2: Paleta Agandada", (iAnchoVentana / 8) * 4, iAltoVentana - 30, 25, poderPunto);
                        break;

                    default:
                        break;
                    }
                }
            }
        }



        DibujarCancha(cancha);
        DibujarPuntaje(jugador1, jugador2, ronda);
        
    }

    void deinit()
    {

    }

    void colisionesPelota(Pelota& pelota, Jugador& jugador1, Jugador& jugador2, int& ronda)
    {
        if (CheckCollisionCircleRec(pelota.posicion, pelota.radio, jugador1.paleta) || CheckCollisionCircleRec(pelota.posicion, pelota.radio, jugador2.paleta))
        {
            if (!pelota.colisionPaleta)
            {
                pelota.velocidadX *= -1;
                if (jugador1.poderTiempo == powerUpTiempo::DireccionInvertida || jugador2.poderTiempo == powerUpTiempo::DireccionInvertida)
                {
                    pelota.velocidadY *= -1;
                }
                pelota.colisionPaleta = true;
                pelota.colisionBorde = colisionPelotaBordes::Ninguno;
            }
        }
        else
        {
            if (ChequeoColisionBordesSup(pelota) && pelota.colisionBorde != colisionPelotaBordes::Superior)
            {
                pelota.velocidadY *= -1;
                pelota.colisionPaleta = false;
                pelota.colisionBorde = colisionPelotaBordes::Superior;
            }
            else
            {
                if (ChequeoColisionBordesInf(pelota) && pelota.colisionBorde != colisionPelotaBordes::Inferior)
                {
                    pelota.velocidadY *= -1;
                    pelota.colisionPaleta = false;
                    pelota.colisionBorde = colisionPelotaBordes::Inferior;
                }
                else
                {
                    if (ChequeoColisionBordesDer(pelota))
                    {
                        if (jugador1.poderPunto == powerUpPunto::Escudo)
                        {
                            pelota.velocidadX *= -1;
                            jugador1.poderPunto = powerUpPunto::Ninguno;

                        }
                        else
                        {
                            if (jugador2.poderPunto == powerUpPunto::AgrandarPaleta)
                            {
                                disminuirTamaņoPaleta(jugador2);
                            }
                            jugador2.poderPunto = (jugador2.poderPunto != powerUpPunto::Ninguno) ? powerUpPunto::Ninguno : jugador2.poderPunto;
                            jugador1.poderPunto = (jugador1.poderPunto == powerUpPunto::MasVelocidad || jugador1.poderPunto == powerUpPunto::MenosVelocidad) ? powerUpPunto::Ninguno : jugador1.poderPunto;
                            jugador2.colorPaleta = confPaletas::pJ2.color;
                            pelota.enMovimiento = false;

                            jugador2.puntuacion++;
                            if (jugador1.poderTiempo != powerUpTiempo::Multiball)
                            {
                                ronda++;
                            }

                            reseteoPelota(pelota);
                        }
                        pelota.colisionBorde = colisionPelotaBordes::Ninguno;
                    }
                    else
                    {
                        if (ChequeoColisionBordesIzq(pelota))
                        {
                            if (jugador2.poderPunto == powerUpPunto::Escudo)
                            {
                                pelota.velocidadX *= -1;
                                jugador2.poderPunto = powerUpPunto::Ninguno;
                            }
                            else
                            {
                                if (jugador1.poderPunto == powerUpPunto::AgrandarPaleta)
                                {
                                    disminuirTamaņoPaleta(jugador1);
                                }
                                jugador1.poderPunto = (jugador1.poderPunto != powerUpPunto::Ninguno) ? powerUpPunto::Ninguno : jugador1.poderPunto;
                                jugador2.poderPunto = (jugador2.poderPunto == powerUpPunto::MasVelocidad || jugador2.poderPunto == powerUpPunto::MenosVelocidad) ? powerUpPunto::Ninguno : jugador2.poderPunto;
                                jugador1.colorPaleta = confPaletas::pJ1.color;
                                pelota.enMovimiento = false;

                                jugador1.puntuacion++;
                                if (jugador1.poderTiempo != powerUpTiempo::Multiball)
                                {
                                    ronda++;
                                }
                                else
                                {
                                    pelota.enMovimiento = false;
                                }
                                reseteoPelota(pelota);

                            }
                            pelota.colisionBorde = colisionPelotaBordes::Ninguno;
                        }
                    }
                }
            }

            if (jugador1.poderTiempo == powerUpTiempo::Obstaculos)
            {
                colisionPelotaObtaculos(jugador1, jugador2, pelota);
            }
        }
    }

    void seteoMultiball(Pelota  pelotas[cantidaPelotas])
    {
        for (short i = 1; i < cantidaPelotas; i++)
        {
            pelotas[i].posicion = pelotas[0].posicion;
            pelotas[i].enMovimiento = true;
            switch (i)
            {
            case 1:
                pelotas[i].velocidadX = pelotas[0].velocidadX * -1;
                pelotas[i].velocidadY = pelotas[0].velocidadY;
                break;
            case 2:
                pelotas[i].velocidadX = pelotas[0].velocidadX;
                pelotas[i].velocidadY = pelotas[0].velocidadY * -1;
                break;
            case 3:
                pelotas[i].velocidadX = pelotas[0].velocidadX * -1;
                pelotas[i].velocidadY = pelotas[0].velocidadY * -1;
                break;
            default:
                break;
            }
        }
    }

    void colisionPelotaObtaculos(Jugador jugador1, Jugador jugador2, Pelota& pelota)
    {
        if (ChequeoColisionSupInfObjeto(jugador1.obstaculo, pelota) || ChequeoColisionSupInfObjeto(jugador2.obstaculo, pelota) && pelota.colisionBorde != colisionPelotaBordes::Obstaculo)
        {
            pelota.colisionBorde = colisionPelotaBordes::Obstaculo;
            pelota.velocidadY *= -1;
        }
        else
        {
            if (ChequeoColisionDerIzqObjeto(jugador1.obstaculo, pelota) || ChequeoColisionDerIzqObjeto(jugador2.obstaculo, pelota) && pelota.colisionBorde != colisionPelotaBordes::Obstaculo)
            {
                pelota.colisionBorde = colisionPelotaBordes::Obstaculo;
                pelota.velocidadX *= -1;
            }
        }
    }

    void seteoObstaculos(Rectangle& obstaculo1, Rectangle& obstaculo2)
    {
        obstaculo1.x = ((iAnchoVentana / 8) * 2) - tamanoObstaculo / 2;
        obstaculo1.y = ((iAltoVentana / 8) * 2) - tamanoObstaculo / 2;
        obstaculo1.width = tamanoObstaculo;
        obstaculo1.height = tamanoObstaculo;

        obstaculo2.x = ((iAnchoVentana / 8) * 6) - tamanoObstaculo / 2;
        obstaculo2.y = ((iAltoVentana / 8) * 6) - tamanoObstaculo / 2;
        obstaculo2.width = tamanoObstaculo;
        obstaculo2.height = tamanoObstaculo;
    }

    powerUpTiempo generarPoderTiempoRandom()
    {
        return (powerUpTiempo)(rand() % cantPWT + 1);

    }

    powerUpPunto generarPoderPuntoRandom()
    {
        return (powerUpPunto)(rand() % cantPWP + 1);
    }
}

