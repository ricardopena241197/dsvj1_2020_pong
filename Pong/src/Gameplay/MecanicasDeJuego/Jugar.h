#ifndef JUGAR_H
#define JUGAR_H

#include "raylib.h"
#include "Ventana/ventana.h"
#include "Configurables/Paletas/paletas.h"
#include "Configurables/teclas_usables.h"
#include "Juego/juego.h"

#include <stdlib.h>
#include <time.h>

namespace jugar
{
    extern bool juegoInicializado;
    enum class powerUpPunto
    {
        Ninguno,
        Escudo,
        AgrandarPaleta,
        MasVelocidad,
        MenosVelocidad
    };
    const int cantPWP = 4;

    enum class powerUpTiempo
    {
        Ninguno,
        Multiball,
        Obstaculos,
        DireccionInvertida,
        ControlesInvertidos
    };
    const int cantPWT = 4;

    enum class colisionPelotaBordes
    {
        Ninguno,
        Superior,
        Inferior,
        Obstaculo,
    };

    const int toleranciaIA = 50;
    const int anchoBordes = 0;
    const int puntajeMax = 10;
    const int diferenciaTamanosPaleta = 50;
    const int cantidaPelotas = 4;
    const int cantidadJugadores = 2;

    const int cambioVelocidad = 3;
    const int velocidadEstandar = 5;
    const int tamanoObstaculo = 40;

    const Color colorBorde = WHITE;
    const Color colorTope = WHITE;

    const Color poderTiempo = DARKGREEN;
    const Color poderPunto = DARKPURPLE;

    struct Jugador
    {
        Rectangle paleta;
        Rectangle obstaculo;
        Color colorPaleta;
        Color colorObstaculo = VIOLET;
        int velocidadPaleta;
        int puntuacion = 0;
        powerUpTiempo poderTiempo = powerUpTiempo::Ninguno;
        powerUpPunto poderPunto = powerUpPunto::Ninguno;
        bool paletaAgrandada = false;
    };

    struct Pelota
    {
        Vector2 posicion;
        int radio = 10;
        int velocidadX = 5;
        int velocidadY = 5;
        bool colisionPaleta = false;
        colisionPelotaBordes colisionBorde = colisionPelotaBordes::Ninguno;
        bool enMovimiento = false;
        bool velocidadAlterada = false;
    };

    struct ParametrosCancha
    {
        Rectangle bordesSupCancha;
        Rectangle bordesInfCancha;
        Rectangle bordesDerCancha;
        Rectangle bordesIzqCancha;
        Color colorCancha{ 230, 41, 55, 255 };
    };

    void mecanicaDeJuegoJvsJ(bool IAActiva);
    void draw();
}

#endif // !JUGAR_H





