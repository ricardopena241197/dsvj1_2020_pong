#include "juego.h"


namespace pong
{
	namespace juego
	{
		Escenas escenaActual=Escenas::MenuPrincipal;
		Escenas escenaAnterior = Escenas::MenuPrincipal;
		void init()
		{
			SetTargetFPS(60);

			srand(time(NULL));

			initControles();

			confPaletas::initPaletas();

			crearVentana();

			SetExitKey(KEY_F1);
		}

		void update()
		{
			switch (escenaActual)
			{
			case Escenas::MenuPrincipal:				
				menu_Inicial::menu();
				break;
			case Escenas::Jugar:
				menu_Jugar::menu();
				break;
			case Escenas::Controles:
				menu_Controles::menu();
				break;
			case Escenas::Creditos:
				menu_Creditos::menu();
				break;
			case Escenas::JvsJ:
				jugar::mecanicaDeJuegoJvsJ(false);
				break;
			case Escenas::JvsIA:
				jugar::mecanicaDeJuegoJvsJ(true);
				break;
			case Escenas::MenuPaletas:
				menu_Paletas::menu();
				break;
			case Escenas::ConfigurarPaletasJugador1:
				menu_Configuracion_Paleta::menu(int(Jugadores::Jugador1));
				break;
			case Escenas::ConfigurarPaletasJugador2:
				menu_Configuracion_Paleta::menu(int(Jugadores::Jugador2));
				break;
			case Escenas::ControlesJugador1:
				menu_Controles_Jugador::menu(int(Jugadores::Jugador1));
				break;
			case Escenas::ControlesJugador2:
				menu_Controles_Jugador::menu(int(Jugadores::Jugador2));
				break;
			case Escenas::Pausa:
				menu_Pausa::menu();
				break;
			default:
				break;
			}
		}

		void draw()
		{
			BeginDrawing();
			switch (escenaActual)
			{
			case Escenas::MenuPrincipal:
				menu_Inicial::draw();
				break;
			case Escenas::Jugar:
				menu_Jugar::draw();
				break;
			case Escenas::Controles:
				menu_Controles::draw();
				break;
			case Escenas::Creditos:
				menu_Creditos::draw();
				break;
			case Escenas::JvsJ:				
			case Escenas::JvsIA:
				jugar::draw();
				break;
			case Escenas::MenuPaletas:
				menu_Paletas::draw();
				break;
			case Escenas::ConfigurarPaletasJugador1:
				menu_Configuracion_Paleta::draw(int(Jugadores::Jugador1));
				break;
			case Escenas::ConfigurarPaletasJugador2:
				menu_Configuracion_Paleta::draw(int(Jugadores::Jugador2));
				break;
			case Escenas::ControlesJugador1:
				menu_Controles_Jugador::draw(int(Jugadores::Jugador1));
				break;
			case Escenas::ControlesJugador2:
				menu_Controles_Jugador::draw(int(Jugadores::Jugador2));
				break;
			case Escenas::Pausa:
				menu_Pausa::draw();
				break;
			default:
				break;
			}
			EndDrawing();
		}

		void deinit()
		{

		}

		void juego()
		{
			init();

			while (!WindowShouldClose() && !auxGlobales::auxSalida)
			{
				update();
				draw();
			}	

			CloseWindow();
		}
	}

}



