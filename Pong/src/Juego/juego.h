#pragma once
#include "raylib.h"
#include "Menus/MenuPrincipal/menu_inicial.h"
#include "Menus/MenuCreditos/menu_creditos.h"
#include "Menus/MenuControles/menu_controles.h"
#include "Menus/MenuJugar/menu_jugar.h"
#include "Menus/MenuPaletas/MenuConfiguracionPaleta/menu_configuracion_paleta.h"
#include "Menus/MenuPaletas/menu_paletas.h"
#include "Menus/MenuPausa/menu_pausa.h"
#include "Gameplay/MecanicasDeJuego/jugar.h"
#include "Ventana/ventana.h"
#include "Configurables/Controles/controles.h"
#include "Configurables/Paletas/paletas.h"
#include "raylib.h"

#include <iostream>
#include <time.h>

using namespace controles;

struct pos
{
	int x = 2;
	int y = 0;
};

enum class Jugadores
{
	Jugador1=1,
	Jugador2
};

enum class Escenas
{
	MenuPrincipal,
	Jugar,
	Controles,
	Creditos,
	JvsJ,
	JvsIA,
	MenuPaletas,
	ConfigurarPaletasJugador1,
	ConfigurarPaletasJugador2,
	ControlesJugador1,
	ControlesJugador2,
	Pausa
};

namespace pong
{
	namespace juego
	{
		extern Escenas escenaActual;
		extern Escenas escenaAnterior;
		void init();
		void update();
		void draw();
		void deinit();
		void juego();
	}	

}




