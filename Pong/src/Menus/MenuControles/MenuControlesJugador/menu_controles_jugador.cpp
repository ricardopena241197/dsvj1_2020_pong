#include "menu_controles_jugador.h"

using namespace menu_Controles_Jugador;

namespace menu_Controles_Jugador
{
    bool menuInicializado = false;
    bool auxEnter=false;

    Color colorOpcion = DARKBLUE;

    int controlSubir ;
    int controlBajar ;

    pos posTitulo;
    pos posOpciones[iCantOpciones];
    int iAuxCursor = 0;

    void init(int jugador)
    {

        controlSubir = (jugador == 1) ? controles::cJ1.subir : controles::cJ2.subir;
        controlBajar = (jugador == 1) ? controles::cJ1.bajar : controles::cJ2.bajar;

        auxEnter = false;

        posTitulo.y = 0;
        posTitulo.x = 2;
        posOpciones[0].y = iAltoVentana / 2;

        for (short i = 1; i < iCantOpciones; i++)
        {
            posOpciones[i].y = posOpciones[i - 1].y + iEspacioEntreOpciones;
        }

        iAuxCursor = 0;
    }

    void update(int jugador)
    {
        if (IsKeyPressed(KEY_ENTER))
        {
            auxEnter = !auxEnter;

        }

        if (auxEnter)
        {
            switch ((opcionesMenu)iAuxCursor)
            {
            case opcionesMenu::Subir:
                if (jugador == 1)
                {
                    controles::cJ1.subir = setearTeclaControles(controles::cJ1.subir, direccionales::subirJ1);
                }
                else
                {
                    controles::cJ2.subir = setearTeclaControles(controles::cJ2.subir, direccionales::subirJ2);
                }
                controlSubir = (jugador == 1) ? controles::cJ1.subir : controles::cJ2.subir;

                break;

            case opcionesMenu::Bajar:
                if (jugador == 1)
                {
                    controles::cJ1.bajar = setearTeclaControles(controles::cJ1.bajar, direccionales::bajarJ1);
                }
                else
                {
                    controles::cJ2.bajar = setearTeclaControles(controles::cJ2.bajar, direccionales::bajarJ2);
                }
                controlBajar = (jugador == 1) ? controles::cJ1.bajar : controles::cJ2.bajar;
                break;

            case opcionesMenu::Salir:
                menuInicializado = false;
                pong::juego::escenaActual = Escenas::Controles;
                break;
            default:
                break;
            }
        }
        else
        {
            if (IsKeyPressed(KEY_UP)) { iAuxCursor = (iAuxCursor == 0) ? iCantOpciones - 1 : iAuxCursor - 1; }
            if (IsKeyPressed(KEY_DOWN)) { iAuxCursor = (iAuxCursor == iCantOpciones - 1) ? 0 : iAuxCursor + 1; }
        }
    }

    void draw(int jugador)
    {
        ClearBackground(RAYWHITE);
        DrawText(TextFormat("Controles Jugador %i", jugador), 2, 0, 50, colorTitulo);

        for (short i = 0; i < iCantOpciones; i++)
        {
            if (iAuxCursor == i)
            {
                colorOpcion = RED;
            }
            else
            {
                colorOpcion = DARKBLUE;
            }
            switch ((opcionesMenu)i)
            {
            case opcionesMenu::Subir:
                if (controlSubir < 262)
                {
                    DrawText(TextFormat("Subir: %c", controlSubir), posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                }
                else
                {
                    switch (controlSubir)
                    {
                    case KEY_UP:
                        DrawText("Subir: Flecha Arriba", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                        break;
                    case KEY_DOWN:
                        DrawText("Subir: Flecha Abajo", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                        break;
                    case KEY_LEFT:
                        DrawText("Subir: Flecha Izquierda", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                        break;
                    case KEY_RIGHT:
                        DrawText("Subir: Flecha Derecha", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                        break;
                    default:
                        break;
                    }
                }


                break;

            case opcionesMenu::Bajar:
                if (controlBajar < 262)
                {
                    DrawText(TextFormat("Bajar: %c", controlBajar), posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                }
                else
                {
                    switch (controlBajar)
                    {
                    case KEY_UP:
                        DrawText("Bajar: Flecha Arriba", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                        break;
                    case KEY_DOWN:
                        DrawText("Bajar: Flecha Abajo", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                        break;
                    case KEY_LEFT:
                        DrawText("Bajar: Flecha Izquierda", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                        break;
                    case KEY_RIGHT:
                        DrawText("Bajar: Flecha Derecha", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                        break;
                    default:
                        break;
                    }
                }
                break;

            case opcionesMenu::Salir:
                DrawText("Salir", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                break;
            default:
                break;
            }

        }

    }

    void deinit()
    {

    }

    void menu(int jugador) {

        if (!menuInicializado)
        {
            init(jugador);
            menuInicializado = true;
        }       
        update(jugador);
    }
}

