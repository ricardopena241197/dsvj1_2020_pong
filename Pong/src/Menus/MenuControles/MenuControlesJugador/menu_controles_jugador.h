#ifndef MENU_CONTROLES_JUGADOR_H
#define MENU_CONTROLES_JUGADOR_H

#include "raylib.h"
#include "Menus/MenuControles/menu_controles.h"
#include "Configurables/Controles/controles.h"
#include "Ventana/ventana.h"

namespace menu_Controles_Jugador
{
	enum class opcionesMenu { Subir, Bajar, Salir };

	const int iEspacioEntreOpciones = ventana::iAltoVentana / 8;
	const int iCantOpciones = 3;
	const Color colorTitulo = BLACK;

	void init(int jugador);

	void update(int jugador);

	void draw(int jugador);

	void deinit();

	void menu(int jugador);
}


#endif // !MENU_CONTROLES_JUGADOR_H

