#pragma once
#include "raylib.h"
#include "Menus/MenuControles/MenuControlesJugador/menu_controles_jugador.h"
#include "Juego/juego.h"
#include "Auxiliares Globales/auxiliares_globales.h"

namespace menu_Controles
{
	enum class opcionesMenu { Jugador1, Jugador2, Salir };	

	const int iEspacioEntreOpciones = 50;
	const int iCantOpciones = 3;
	const Color colorTitulo = BLACK;

	void init();

	void update();

	void draw();

	void deinit();

	void menu();
}
