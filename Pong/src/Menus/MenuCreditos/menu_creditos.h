#pragma once

#include "raylib.h"
#include "Juego/juego.h"

namespace menu_Creditos
{
	enum class opcionesMenu { Salir };

	const int iEspacioEntreOpciones = 50;
	const int iCantOpciones = 1;
	const Color colorTitulo = BLACK;

	void init();

	void update();

	void draw();

	void deinit();

	void menu();
}