#pragma once
#include "raylib.h"
#include "Juego/juego.h"

namespace menu_Jugar
{
	enum class opcionesMenu { JvsJ, JvsIA, ConfigPaletas, Salir };
	const int iEspacioEntreOpciones = 50;
	const int iCantOpciones = 4;
	const Color colorTitulo = BLACK;

	void init();

	void update();

	void draw();

	void deinit();

	void menu();

}