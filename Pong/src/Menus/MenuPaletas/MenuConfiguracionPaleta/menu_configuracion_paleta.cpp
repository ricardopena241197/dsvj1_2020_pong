#include "menu_configuracion_paleta.h"

using namespace confPaletas;

namespace menu_Configuracion_Paleta
{
    bool menuInicializado = false;
    bool auxEnter = false;
    bool opcionSelecionada = false;

    Color colorOpcion = DARKBLUE;
    Color colorFlechas = DARKBLUE;

    Rectangle paletaMostrada;
    paletas paletaJugador;

    pos posTitulo;
    pos posOpciones[iCantOpciones];

    int iAuxCursor = 0;
    int cursorColor;
    int cursorTamano;

    void init(int jugador)
    {

        paletaJugador = (jugador == 1) ? confPaletas::pJ1 : confPaletas::pJ2;
        cursorColor = (int)paletaJugador.idColor;
        cursorTamano = (int)paletaJugador.tamano;

        setearPaletaMostrada(paletaMostrada, paletaJugador.tamano);

        auxEnter = false;
        opcionSelecionada = false;
        colorFlechas = DARKBLUE;

        posTitulo.y = 0;
        posTitulo.x = 2;
        posOpciones[0].y = iAltoVentana / 4;

        for (short i = 1; i < iCantOpciones; i++)
        {
            posOpciones[i].y = posOpciones[i - 1].y + iEspacioEntreOpciones;
        }

        iAuxCursor = 0;
    }

    void update(int jugador)
    {
        if (IsKeyPressed(KEY_ENTER))
        {
            auxEnter = !auxEnter;
            opcionSelecionada = !opcionSelecionada;
            if (opcionSelecionada)
            {
                colorFlechas = RED;
            }
            else
            {
                colorFlechas = DARKBLUE;
            }
        }

        if (auxEnter)
        {
            switch ((opcionesMenu)iAuxCursor)
            {
            case opcionesMenu::Color:

                if (IsKeyPressed(KEY_RIGHT))
                {
                    cursorColor = (cursorColor == iCantColores - 1) ? 0 : cursorColor + 1;
                }
                if (IsKeyPressed(KEY_LEFT))
                {
                    cursorColor = (cursorColor == 0) ? iCantColores - 1 : cursorColor - 1;
                }
                paletaJugador.idColor = (identificadorColor)cursorColor;
                setearColorPaleta(paletaJugador.idColor, paletaJugador.color);
                break;

            case opcionesMenu::Tamano:

                if (IsKeyPressed(KEY_RIGHT))
                {
                    cursorTamano = (cursorTamano == iCantTamanos - 1) ? 0 : cursorTamano + 1;

                }
                if (IsKeyPressed(KEY_LEFT))
                {
                    cursorTamano = (cursorTamano == 0) ? iCantTamanos - 1 : cursorTamano - 1;

                }
                paletaJugador.tamano = (tamanos)cursorTamano;
                setearPaletaMostrada(paletaMostrada, paletaJugador.tamano);
                break;

                break;
            case opcionesMenu::Salir:
                menuInicializado = false;
                pong::juego::escenaActual = Escenas::MenuPaletas;
                if (jugador == 1)
                {
                    confPaletas::pJ1 = paletaJugador;
                }
                else
                {
                    confPaletas::pJ2 = paletaJugador;
                }

                break;
            default:
                break;
            }
        }
        else
        {
            if (IsKeyPressed(KEY_UP)) { iAuxCursor = (iAuxCursor == 0) ? iCantOpciones - 1 : iAuxCursor - 1; }
            if (IsKeyPressed(KEY_DOWN)) { iAuxCursor = (iAuxCursor == iCantOpciones - 1) ? 0 : iAuxCursor + 1; }
        }
    }

    void draw(int jugador)
    {
        ClearBackground(RAYWHITE);

        DrawText(TextFormat("Paleta Jugador %i", jugador), 2, 0, 50, colorTitulo);
        DrawRectangleRec(paletaMostrada, paletaJugador.color);
        DrawText("<", paletaMostrada.x - anchoTodasPaletas - 10, paletaMostrada.y + paletaMostrada.height / 2 - 20, 40, colorFlechas);
        DrawText(">", paletaMostrada.x + anchoTodasPaletas + 10, paletaMostrada.y + paletaMostrada.height / 2 - 20, 40, colorFlechas);

        for (short i = 0; i < iCantOpciones; i++)
        {
            if (iAuxCursor == i)
            {
                colorOpcion = RED;
            }
            else
            {
                colorOpcion = DARKBLUE;
            }
            switch ((opcionesMenu)i)
            {
            case opcionesMenu::Color:
                DrawText("Color", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);

                break;

            case opcionesMenu::Tamano:
                DrawText("Tamano", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);

                break;

            case opcionesMenu::Salir:
                DrawText("Salir", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                break;
            default:
                break;
            }

        }

    }

    void deinit()
    {

    }

    void menu(int jugador) {

        if (!menuInicializado)
        {
            init(jugador);
            menuInicializado = true;
        }
        update(jugador);
    }
}

