#pragma once
#include "raylib.h"
#include "Configurables/Paletas/paletas.h"
#include "Juego/juego.h"

namespace menu_Configuracion_Paleta
{
	enum class opcionesMenu { Color, Tamano, Salir };

	const int iEspacioEntreOpciones = iAltoVentana / 4;
	const int iCantOpciones = 3;
	const Color colorTitulo = BLACK;

	void init(int jugador);

	void update(int jugador);

	void draw(int jugador);

	void deinit();

	void menu(int jugador);
}