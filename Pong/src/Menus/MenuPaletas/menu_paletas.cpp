#include "menu_paletas.h"

namespace menu_Paletas
{
    bool menuInicializado = false;
    Color colorOpcion = DARKBLUE;

    pos posTitulo;
    pos posOpciones[iCantOpciones];
    int iAuxCursor = 0;

    void init()
    {
        posTitulo.y = 0;
        posTitulo.x = 2;
        posOpciones[0].y = iAltoVentana / 4;

       

        for (short i = 1; i < iCantOpciones; i++)
        {
            posOpciones[i].y = posOpciones[i - 1].y + iEspacioEntreOpciones;
        }

        iAuxCursor = 0;
    }

    void update()
    {
        if (IsKeyPressed(KEY_UP)) { iAuxCursor = (iAuxCursor == 0) ? iCantOpciones - 1 : iAuxCursor - 1; }
        if (IsKeyPressed(KEY_DOWN)) { iAuxCursor = (iAuxCursor == iCantOpciones - 1) ? 0 : iAuxCursor + 1; }
        if (IsKeyPressed(KEY_ENTER))
        {
            menuInicializado = false;
            switch ((opcionesMenu)iAuxCursor)
            {
            case opcionesMenu::Jugador1:

                pong::juego::escenaActual = Escenas::ConfigurarPaletasJugador1;

                break;
            case opcionesMenu::Jugador2:

                pong::juego::escenaActual = Escenas::ConfigurarPaletasJugador2;

                break;
            case opcionesMenu::Salir:
                menuInicializado = false;
                pong::juego::escenaActual = Escenas::Jugar;

                break;
            default:
                break;
            }
        }
    }

    void draw()
    {
        ClearBackground(RAYWHITE);

        DrawText("Paletas", posTitulo.y, posTitulo.y, 50, colorTitulo);

        for (short i = 0; i < iCantOpciones; i++)
        {
            if (iAuxCursor == i)
            {
                colorOpcion = RED;
            }
            else
            {
                colorOpcion = DARKBLUE;
            }
            switch ((opcionesMenu)i)
            {
            case opcionesMenu::Jugador1:
                DrawText("Jugador 1 ", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);

                break;

            case opcionesMenu::Jugador2:
                DrawText("Jugador 2", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);

                break;

            case opcionesMenu::Salir:
                DrawText("Salir", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                break;
            default:
                break;
            }

        }


    }

    void deinit()
    {

    }

    void menu() {

        if (!menuInicializado)
        {
            init();
            menuInicializado = true;
        }
        update();

    }
}