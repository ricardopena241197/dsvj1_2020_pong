#ifndef MENU_PALETAS_H
#define MENU_PALETAS_H

#include "raylib.h"
#include "Juego/juego.h"


namespace menu_Paletas
{
	enum class opcionesMenu { Jugador1, Jugador2, Salir };

	const int iEspacioEntreOpciones = iAltoVentana / 4;
	const int iCantOpciones = 3;
	const Color colorTitulo = BLACK;

	void init();

	void update();

	void draw();

	void deinit();

	void menu();
}


#endif // !MENU_PALETAS_H

