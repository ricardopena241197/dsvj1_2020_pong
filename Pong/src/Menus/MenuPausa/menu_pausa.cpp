#include "menu_pausa.h"

namespace menu_Pausa
{
    bool menuInicializado = false;
    Color colorOpcion = DARKBLUE;

    pos posTitulo;
    pos posOpciones[iCantOpciones];
    int iAuxCursor = 0;

    void init()
    {
        posTitulo.y = 0;
        posTitulo.x = 2;
        posOpciones[0].y = iAltoVentana / 2;

        for (short i = 1; i < iCantOpciones; i++)
        {
            posOpciones[i].y = posOpciones[i - 1].y + iEspacioEntreOpciones;
        }

        iAuxCursor = 0;
    }

    void update()
    {
        if (IsKeyPressed(KEY_UP)) { iAuxCursor = (iAuxCursor == 0) ? iCantOpciones - 1 : iAuxCursor - 1; }
        if (IsKeyPressed(KEY_DOWN)) { iAuxCursor = (iAuxCursor == iCantOpciones - 1) ? 0 : iAuxCursor + 1; }
        if (IsKeyPressed(KEY_ENTER))
        {
            menuInicializado = false;
            switch ((opcionesMenu)iAuxCursor)
            {
            case opcionesMenu::Continuar:
                pong::juego::escenaActual = pong::juego::escenaAnterior;
                break;
            case opcionesMenu::Salir:
                jugar::juegoInicializado = false;
                pong::juego::escenaActual = Escenas::Jugar;
                break;
            default:
                break;
            }

        }
    }

    void draw()
    {        
        ClearBackground(RAYWHITE);

        DrawText("Juego en Pausa", posTitulo.x, posTitulo.y, 50, colorTitulo);

        for (short i = 0; i < iCantOpciones; i++)
        {
            if (iAuxCursor == i)
            {
                colorOpcion = RED;
            }
            else
            {
                colorOpcion = DARKBLUE;
            }
            switch ((opcionesMenu)i)
            {
            case opcionesMenu::Continuar:
                DrawText("Continuar", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                break;
            case opcionesMenu::Salir:
                DrawText("Salir", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                break;
            default:
                break;
            }

        }
    }

    void deinit()
    {

    }

    void menu()
    {
        if (!menuInicializado)
        {
            init();
            menuInicializado = true;
        }

        update();

    }
}