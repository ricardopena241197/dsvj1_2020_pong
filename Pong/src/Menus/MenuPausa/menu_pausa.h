#pragma once
#include "raylib.h"
#include "Juego/juego.h"
#include "Auxiliares Globales/auxiliares_globales.h"
#include "Gameplay/MecanicasDeJuego/Jugar.h"

namespace menu_Pausa
{
	enum class opcionesMenu { Continuar, Salir };
	const int iEspacioEntreOpciones = 50;
	const int iCantOpciones = 2;
	const Color colorTitulo = BLACK;

	void init();

	void update();

	void draw();

	void deinit();

	void menu();
}