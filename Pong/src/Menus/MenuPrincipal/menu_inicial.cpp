#include "menu_inicial.h"

namespace menu_Inicial
{
    bool menuInicializado = false;
    Color colorOpcion = DARKBLUE;

    pos posTitulo;
    pos posOpciones[iCantOpciones];
    int iAuxCursor = 0;

    void init()
    {
        posTitulo.y = 0;
        posTitulo.x = 2;
        posOpciones[0].y = iAltoVentana / 2;

        for (short i = 1; i < iCantOpciones; i++)
        {
            posOpciones[i].y = posOpciones[i - 1].y + iEspacioEntreOpciones;
        }

        iAuxCursor = 0;
    }

    void update()
    {
        if (IsKeyPressed(KEY_UP)) { iAuxCursor = (iAuxCursor == 0) ? iCantOpciones - 1 : iAuxCursor - 1; }
        if (IsKeyPressed(KEY_DOWN)) { iAuxCursor = (iAuxCursor == iCantOpciones - 1) ? 0 : iAuxCursor + 1; }
        if (IsKeyPressed(KEY_ENTER))
        {
            menuInicializado = false;
            switch ((opcionesMenu)iAuxCursor)
            {
            case opcionesMenu::Jugar:
                pong::juego::escenaActual = Escenas::Jugar;
                break;
            case opcionesMenu::Controles:
                pong::juego::escenaActual = Escenas::Controles;
                break;
            case opcionesMenu::Creditos:
                pong::juego::escenaActual = Escenas::Creditos;
                break;
            case opcionesMenu::Salir:
                auxGlobales::auxSalida = true;
                break;
            default:
                break;
            }

        }
    }

    void draw()
    {        
        ClearBackground(RAYWHITE);
        DrawText("Menu Principal", posTitulo.x, posTitulo.y, 50, colorTitulo);

        for (short i = 0; i < iCantOpciones; i++)
        {
            if (iAuxCursor == i)
            {
                colorOpcion = RED;
            }
            else
            {
                colorOpcion = DARKBLUE;
            }
            switch ((opcionesMenu)i)
            {
            case opcionesMenu::Jugar:
                DrawText("Jugar", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                break;
            case opcionesMenu::Controles:
                DrawText("Controles", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                break;
            case opcionesMenu::Creditos:
                DrawText("Creditos", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                break;
            case opcionesMenu::Salir:
                DrawText("Salir", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                break;
            default:
                break;
            }
        }        
    }

    void deinit()
    {

    }

    void menu() 
    {
        if (!menuInicializado)
        {
            init();
            menuInicializado = true;
        }   

        update();
        
    }
}

