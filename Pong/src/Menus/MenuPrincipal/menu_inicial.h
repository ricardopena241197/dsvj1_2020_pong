#pragma once
#include "raylib.h"
#include "Juego/juego.h"
#include "Auxiliares Globales/auxiliares_globales.h"

namespace menu_Inicial
{
	enum class opcionesMenu { Jugar, Controles, Creditos, Salir };
	const int iEspacioEntreOpciones = 50;
	const int iCantOpciones = 4;
	const Color colorTitulo = BLACK;	

	void init();

	void update();

	void draw();

	void deinit();

	void menu();
}
