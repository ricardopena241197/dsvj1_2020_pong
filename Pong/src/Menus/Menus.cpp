#include "Menus.h"

/* 
void menuConfiguracionPaletas(int jugador) 
{
    enum class opcionesMenu { Color, Tamano, Salir };
    const int iEspacioEntreOpciones = iAltoVentana / 4;
    const int iCantOpciones = 3;
    

    Color colorTitulo = BLACK;
    Color colorOpcion = DARKBLUE;
    Color colorFlechas = DARKBLUE;

    pos posTitulo;
    posTitulo.y = 0;
    pos posOpciones[iCantOpciones];
    posOpciones[0].y = iAltoVentana / 4;

    Rectangle paletaMostrada;

    paletas paletaJugador = (jugador == 1) ? confPaletas::pJ1 : confPaletas::pJ2;

    setearPaletaMostrada(paletaMostrada, paletaJugador.tamano);   

    bool auxEnter = false;
    bool bAuxSalida = false;
    bool opcionSelecionada = false;
    int iAuxCursor = 0;
    int cursorColor = (int)paletaJugador.idColor;
    int cursorTamano = (int)paletaJugador.tamano;

    for (short i = 1; i < iCantOpciones; i++)
    {
        posOpciones[i].y = posOpciones[i - 1].y + iEspacioEntreOpciones;
    }

    while (!WindowShouldClose() && !bAuxSalida) {

        BeginDrawing();
        ClearBackground(RAYWHITE);
        
        DrawText(TextFormat("Paleta Jugador %i", jugador), 2, 0, 50, colorTitulo);
        DrawRectangleRec(paletaMostrada, paletaJugador.color);
        DrawText("<", paletaMostrada.x - anchoTodasPaletas-10, paletaMostrada.y+paletaMostrada.height/2-20, 40, colorFlechas);
        DrawText(">", paletaMostrada.x + anchoTodasPaletas+10, paletaMostrada.y + paletaMostrada.height / 2-20, 40, colorFlechas);

        for (short i = 0; i < iCantOpciones; i++)
        {
            if (iAuxCursor == i)
            {
                colorOpcion = RED;
            }
            else
            {
                colorOpcion = DARKBLUE;
            }
            switch ((opcionesMenu)i)
            {
            case opcionesMenu::Color:
                DrawText("Color", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);

                break;

            case opcionesMenu::Tamano:
                DrawText("Tamano", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);

                break;

            case opcionesMenu::Salir:
                DrawText("Salir", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                break;
            default:
                break;
            }

        }
        EndDrawing();
        if (auxEnter)
        {
            switch ((opcionesMenu)iAuxCursor)
            {
            case opcionesMenu::Color:
                        
                if (IsKeyPressed(KEY_RIGHT))
                { 
                    cursorColor = (cursorColor == iCantColores-1) ? 0 : cursorColor+1;
                }
                if (IsKeyPressed(KEY_LEFT)) 
                { 
                   cursorColor = (cursorColor == 0) ? iCantColores - 1 : cursorColor-1;
                }
                paletaJugador.idColor = (identificadorColor)cursorColor;
                setearColorPaleta(paletaJugador.idColor, paletaJugador.color);
                break;

            case opcionesMenu::Tamano:
                
                if (IsKeyPressed(KEY_RIGHT))
                {
                    cursorTamano = (cursorTamano == iCantTamanos - 1) ? 0 : cursorTamano + 1;
                    
                }
                if (IsKeyPressed(KEY_LEFT))
                {
                    cursorTamano = (cursorTamano == 0) ? iCantTamanos - 1 : cursorTamano - 1;
                   
                }
                paletaJugador.tamano = (tamanos)cursorTamano;               
                setearPaletaMostrada(paletaMostrada, paletaJugador.tamano);                
                break;
                
                break;
            case opcionesMenu::Salir:
                bAuxSalida = true;
                if (jugador==1)
                {
                    confPaletas::pJ1 = paletaJugador;
                }
                else
                {
                    confPaletas::pJ2 = paletaJugador;
                }               

                break;
            default:
                break;
            }
        }
        else
        {
            if (IsKeyPressed(KEY_UP)) { iAuxCursor = (iAuxCursor == 0) ? iCantOpciones - 1 : iAuxCursor - 1; }
            if (IsKeyPressed(KEY_DOWN)) { iAuxCursor = (iAuxCursor == iCantOpciones - 1) ? 0 : iAuxCursor + 1; }
            
        }

        if (IsKeyPressed(KEY_ENTER))
        {
            auxEnter = !auxEnter;
            opcionSelecionada = !opcionSelecionada;
            if (opcionSelecionada)
            {
                colorFlechas = RED;
            }
            else
            {
                colorFlechas = DARKBLUE;
            }            
        }
    }
}

void menuPaletas() 
{
    enum class opcionesMenu { Jugador1, Jugador2, Salir };
    const int iEspacioEntreOpciones = iAltoVentana / 4;
    const int iCantOpciones = 3;
    Color colorTitulo = BLACK;
    Color colorOpcion = DARKBLUE;

    pos posTitulo;
    posTitulo.y = 0;
    posTitulo.x = 2;
    pos posOpciones[iCantOpciones];
    posOpciones[0].y = iAltoVentana / 4;

    for (short i = 1; i < iCantOpciones; i++)
    {
        posOpciones[i].y = posOpciones[i - 1].y + iEspacioEntreOpciones;
    }

    bool bAuxSalida = false;
    int iAuxCursor = 0;

    while (!WindowShouldClose() && !bAuxSalida) {

        BeginDrawing();
        ClearBackground(RAYWHITE);

        DrawText("Paletas", posTitulo.y, posTitulo.y, 50, colorTitulo);

        for (short i = 0; i < iCantOpciones; i++)
        {
            if (iAuxCursor == i)
            {
                colorOpcion = RED;
            }
            else
            {
                colorOpcion = DARKBLUE;
            }
            switch ((opcionesMenu)i)
            {
            case opcionesMenu::Jugador1:
                DrawText("Jugador 1 ", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);

                break;

            case opcionesMenu::Jugador2:
                DrawText("Jugador 2", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);

                break;

            case opcionesMenu::Salir:
                DrawText("Salir", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                break;
            default:
                break;
            }

        }


        EndDrawing();

        if (IsKeyPressed(KEY_UP)) { iAuxCursor = (iAuxCursor == 0) ? iCantOpciones - 1 : iAuxCursor - 1; }
        if (IsKeyPressed(KEY_DOWN)) { iAuxCursor = (iAuxCursor == iCantOpciones - 1) ? 0 : iAuxCursor + 1; }

        if (IsKeyPressed(KEY_ENTER))
        {
            switch ((opcionesMenu)iAuxCursor)
            {
            case opcionesMenu::Jugador1:
                menuConfiguracionPaletas(int(opcionesMenu::Jugador1) + 1);
                break;
            case opcionesMenu::Jugador2:
                menuConfiguracionPaletas(int(opcionesMenu::Jugador2) + 1);
                break;
            case opcionesMenu::Salir:
                bAuxSalida = true;
                break;
            default:
                break;
            }

        }

    }

}

void menuControlesJugador(int jugador) 
{
    enum class opcionesMenu { Subir, Bajar, Salir };
    const int iEspacioEntreOpciones = iAltoVentana / 4;
    const int iCantOpciones = 3;
    Color colorTitulo = BLACK;
    Color colorOpcion = DARKBLUE;    
    int controlSubir = (jugador == 1) ? controles::cJ1.subir: controles::cJ2.subir;
    int controlBajar = (jugador == 1) ? controles::cJ1.bajar : controles::cJ2.bajar;
    

    pos posTitulo;
    posTitulo.y = 0;
    pos posOpciones[iCantOpciones];
    posOpciones[0].y = iAltoVentana / 4;

    for (short i = 1; i < iCantOpciones; i++)
    {
        posOpciones[i].y = posOpciones[i - 1].y + iEspacioEntreOpciones;
    }

    bool auxEnter = false;
    bool bAuxSalida = false;
    int iAuxCursor = 0;
    

    while (!WindowShouldClose() && !bAuxSalida) {

        BeginDrawing();
        ClearBackground(RAYWHITE);

        DrawText(TextFormat("Controles Jugador %i", jugador), 2, 0, 50, colorTitulo);
        
        for (short i = 0; i < iCantOpciones; i++)
        {
            if (iAuxCursor == i )
            {
                colorOpcion = RED;
            }
            else
            {
                colorOpcion = DARKBLUE;
            }
            switch ((opcionesMenu)i)
            {
            case opcionesMenu::Subir:
                if (controlSubir < 262)
                {
                    DrawText(TextFormat("Subir: %c", controlSubir), posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                }
                else
                {
                    switch (controlSubir)
                    {
                    case KEY_UP:
                        DrawText("Subir: Flecha Arriba", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                        break;
                    case KEY_DOWN:
                        DrawText("Subir: Flecha Abajo", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                        break;
                    case KEY_LEFT:
                        DrawText("Subir: Flecha Izquierda", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                        break;
                    case KEY_RIGHT:
                        DrawText("Subir: Flecha Derecha", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                        break;
                    default:
                        break;
                    }
                }
                               

                break;

            case opcionesMenu::Bajar:
                if (controlBajar < 262)
                {
                    DrawText(TextFormat("Bajar: %c", controlBajar), posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                }
                else
                {
                    switch (controlBajar)
                    {
                    case KEY_UP:
                        DrawText("Bajar: Flecha Arriba", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                        break;
                    case KEY_DOWN:
                        DrawText("Bajar: Flecha Abajo", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                        break;
                    case KEY_LEFT:
                        DrawText("Bajar: Flecha Izquierda", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                        break;
                    case KEY_RIGHT:
                        DrawText("Bajar: Flecha Derecha", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                        break;
                    default:
                        break;
                    }
                }
                break;

            case opcionesMenu::Salir:
                DrawText("Salir", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                break;
            default:
                break;
            }

        }
        EndDrawing();
        if (auxEnter)  
        {
            
            switch ((opcionesMenu)iAuxCursor)
            {
            case opcionesMenu::Subir:
                if (jugador==1)
                {
                    controles::cJ1.subir = setearTeclaControles(controles::cJ1.subir,direccionales::subirJ1);
                }
                else
                {
                    controles::cJ2.subir = setearTeclaControles(controles::cJ2.subir, direccionales::subirJ2);
                }
                controlSubir = (jugador == 1) ? controles::cJ1.subir : controles::cJ2.subir;
                
                break;

            case opcionesMenu::Bajar:
                if (jugador == 1)
                {
                    controles::cJ1.bajar = setearTeclaControles(controles::cJ1.bajar, direccionales::bajarJ1);
                }
                else
                {
                    controles::cJ2.bajar = setearTeclaControles(controles::cJ2.bajar, direccionales::bajarJ2);
                }
                controlBajar = (jugador == 1) ? controles::cJ1.bajar : controles::cJ2.bajar;
                break;
            case opcionesMenu::Salir:
                bAuxSalida = true;
                break;
            default:
                break;
            }
        }
        else
        {
            if (IsKeyPressed(KEY_UP)) { iAuxCursor = (iAuxCursor == 0) ? iCantOpciones - 1 : iAuxCursor - 1; }
            if (IsKeyPressed(KEY_DOWN)) { iAuxCursor = (iAuxCursor == iCantOpciones - 1) ? 0 : iAuxCursor + 1; }
        }
        
        
        if (IsKeyPressed(KEY_ENTER))
        {      
            auxEnter = !auxEnter;    

        }
        
       

    }

}

void menuControles() 
{
    enum class opcionesMenu { Jugador1,Jugador2, Salir };
    const int iEspacioEntreOpciones = iAltoVentana / 4;
    const int iCantOpciones = 3;
    Color colorTitulo = BLACK;
    Color colorOpcion = DARKBLUE;    

    pos posTitulo;
    posTitulo.y = 0;
    posTitulo.x = 2;
    pos posOpciones[iCantOpciones];
    posOpciones[0].y = iAltoVentana / 4;

    for (short i = 1; i < iCantOpciones; i++)
    {
        posOpciones[i].y = posOpciones[i - 1].y + iEspacioEntreOpciones;
    }

    bool bAuxSalida = false;
    int iAuxCursor = 0;

    while (!WindowShouldClose() && !bAuxSalida) {
               
        BeginDrawing();
        ClearBackground(RAYWHITE);

        DrawText("Controles", posTitulo.y, posTitulo.y, 50, colorTitulo);

        for (short i = 0; i < iCantOpciones; i++)
        {
            if (iAuxCursor == i)
            {
                colorOpcion = RED;
            }
            else
            {
                colorOpcion = DARKBLUE;
            }
            switch ((opcionesMenu)i)
            {            
            case opcionesMenu::Jugador1:
                DrawText("Jugador 1 ", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);

                break;
           
            case opcionesMenu::Jugador2:
                DrawText("Jugador 2", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                               
                break;           
            
            case opcionesMenu::Salir:
                DrawText("Salir", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                break;
            default:
                break;
            }

        }
       

        EndDrawing();

        if (IsKeyPressed(KEY_UP)) { iAuxCursor = (iAuxCursor == 0) ? iCantOpciones - 1 : iAuxCursor - 1; }
        if (IsKeyPressed(KEY_DOWN)) { iAuxCursor = (iAuxCursor == iCantOpciones - 1) ? 0 : iAuxCursor + 1; }
        
        if (IsKeyPressed(KEY_ENTER))
        {
            switch ((opcionesMenu)iAuxCursor)
            {
            case opcionesMenu::Jugador1:
                menuControlesJugador(int(opcionesMenu::Jugador1) + 1);
                break;
            case opcionesMenu::Jugador2:
                menuControlesJugador(int(opcionesMenu::Jugador2) + 1);
                break;
            case opcionesMenu::Salir:
                bAuxSalida = true;
                break;
            default:
                break;
            }
        }        
    }
}

void menuJuego() 
{
    enum class opcionesMenu { JvsJ, JvsCpu, ConfigPaletas, Salir };
    const int iEspacioEntreOpciones = 50;
    const int iCantOpciones = 4;
    Color colorTitulo = BLACK;
    Color colorOpcion = DARKBLUE;

    pos posTitulo;
    posTitulo.y = 0;
    posTitulo.x = 2;
    pos posOpciones[iCantOpciones];
    posOpciones[0].y = iAltoVentana / 2;

    for (short i = 1; i < iCantOpciones; i++)
    {
        posOpciones[i].y = posOpciones[i - 1].y + iEspacioEntreOpciones;
    }

    bool IAActiva = false;
    bool bAuxSalida = false;
    int iAuxCursor = 0;

    while (!WindowShouldClose() && !bAuxSalida) {

        BeginDrawing();
        ClearBackground(RAYWHITE);

        DrawText("Jugar", posTitulo.x, posTitulo.y, 50, colorTitulo);

        for (short i = 0; i < iCantOpciones; i++)
        {
            if (iAuxCursor == i)
            {
                colorOpcion = RED;
            }
            else
            {
                colorOpcion = DARKBLUE;
            }
            switch ((opcionesMenu)i)
            {
            case opcionesMenu::JvsJ:
                DrawText("Jugar: JvsJ", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                break;
            case opcionesMenu::JvsCpu:
                DrawText("Jugar: JvsIA", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                break;
            case opcionesMenu::ConfigPaletas:
                DrawText("Cofigurar Paletas", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                break;
            case opcionesMenu::Salir:
                DrawText("Salir", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                break;
            default:
                break;
            }

        }

        EndDrawing();

        if (IsKeyPressed(KEY_UP)) { iAuxCursor = (iAuxCursor == 0) ? iCantOpciones - 1 : iAuxCursor - 1; }
        if (IsKeyPressed(KEY_DOWN)) { iAuxCursor = (iAuxCursor == iCantOpciones - 1) ? 0 : iAuxCursor + 1; }
        if (IsKeyPressed(KEY_ENTER))
        {
            switch ((opcionesMenu)iAuxCursor)
            {
            case opcionesMenu::JvsJ:
                IAActiva = false;
                //MecanicaDeJuegoJvsJ(IAActiva);
                break;
            case opcionesMenu::JvsCpu:
                IAActiva = true;
                //MecanicaDeJuegoJvsJ(IAActiva);
                break;
            case opcionesMenu::ConfigPaletas:
                menuPaletas();
                break;
            case opcionesMenu::Salir:
                bAuxSalida = true;
                break;
            default:
                break;
            }            

        }

    }

}

void pruebaImput() {

    while (!WindowShouldClose()) {

        BeginDrawing();
        ClearBackground(RAYWHITE);

        DrawText(TextFormat("Imput: %i", GetKeyPressed()), 2, iAltoVentana / 2,100, BLACK);

        EndDrawing();
    }
}


void menuCreditos() 
{
    enum class opcionesMenu { Salir };
    const int iEspacioEntreOpciones = 50;
    const int iCantOpciones = 1;
    Color colorTitulo = BLACK;
    Color colorOpcion = DARKBLUE;

    pos posTitulo;
    posTitulo.y = 0;
    posTitulo.x = 2;
    pos posOpciones[iCantOpciones];
    posOpciones[0].y = (iAltoVentana / 5)*4;

    bool bAuxSalida = false;
    int iAuxCursor = 0;

    while (!WindowShouldClose() && !bAuxSalida) {

        BeginDrawing();
        ClearBackground(RAYWHITE);

        DrawText("Creado por:", posTitulo.x, posTitulo.y, 100, colorTitulo);
        DrawText("Ricardo Pena", posTitulo.x, posTitulo.y+100, 100, colorTitulo);

        for (short i = 0; i < iCantOpciones; i++)
        {
            if (iAuxCursor == i)
            {
                colorOpcion = RED;
            }
            else
            {
                colorOpcion = DARKBLUE;
            }
            switch ((opcionesMenu)i)
            {
            
            case opcionesMenu::Salir:
                DrawText("Salir", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                break;
            default:
                break;
            }

        }

        EndDrawing();

        if (IsKeyPressed(KEY_UP)) { iAuxCursor = (iAuxCursor == 0) ? iCantOpciones - 1 : iAuxCursor - 1; }
        if (IsKeyPressed(KEY_DOWN)) { iAuxCursor = (iAuxCursor == iCantOpciones - 1) ? 0 : iAuxCursor + 1; }
        if (IsKeyPressed(KEY_ENTER))
        {
            switch ((opcionesMenu)iAuxCursor)
            {           
            case opcionesMenu::Salir:
                bAuxSalida = true;
                break;
            default:
                break;
            }

        }

    }

}

void menuPrincipal() 
{
    enum class opcionesMenu { Jugar, Controles,Creditos, Salir };
    const int iEspacioEntreOpciones = 50;
    const int iCantOpciones = 4;
    Color colorTitulo = BLACK;
    Color colorOpcion = DARKBLUE;
    
    pos posTitulo;
    posTitulo.y = 0;
    posTitulo.x = 2;
    pos posOpciones[iCantOpciones];
    posOpciones[0].y = iAltoVentana / 2;
    
    for (short i = 1; i < iCantOpciones; i++)
    {
        posOpciones[i].y= posOpciones[i-1].y+iEspacioEntreOpciones;
    }

    bool bAuxSalida = false;
    int iAuxCursor = 0;

    while (!WindowShouldClose()&&!bAuxSalida) {

        BeginDrawing();
        ClearBackground(RAYWHITE); 

        DrawText("Menu Principal", posTitulo.x, posTitulo.y, 50, colorTitulo);

        for (short i = 0; i < iCantOpciones; i++)
        {
            if (iAuxCursor==i)
            {
                colorOpcion = RED;
            }
            else
            {
                colorOpcion = DARKBLUE;
            }
            switch ((opcionesMenu)i)
            {
            case opcionesMenu::Jugar:
                DrawText("Jugar", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                break;
            case opcionesMenu::Controles:
                DrawText("Controles", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                break;
            case opcionesMenu::Creditos:
                DrawText("Creditos", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                break;
            case opcionesMenu::Salir:
                DrawText("Salir", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                break;
            default:
                break;
            }

        }        

        EndDrawing();

        if (IsKeyPressed(KEY_UP))       {iAuxCursor = (iAuxCursor==0) ? iCantOpciones - 1 : iAuxCursor - 1;}
        if (IsKeyPressed(KEY_DOWN))     {iAuxCursor = (iAuxCursor == iCantOpciones - 1) ? 0 : iAuxCursor + 1;}
        if (IsKeyPressed(KEY_ENTER))
        {
            switch ((opcionesMenu)iAuxCursor)
            {
            case opcionesMenu::Jugar:                
                menuJuego();
                break;
            case opcionesMenu::Controles:
                menuControles();
                break;
            case opcionesMenu::Creditos:
                menuCreditos();
                break;
            case opcionesMenu::Salir:
                bAuxSalida = true;
                break;
            default:
                break;
            }

        }

    }   

}

bool menuPausa()
{
    enum class opcionesMenu { Continuar, Salir };
    const int iEspacioEntreOpciones = 50;
    const int iCantOpciones = 2;
    Color colorTitulo = BLACK;
    Color colorOpcion = DARKBLUE;

    pos posTitulo;
    posTitulo.y = 0;
    posTitulo.x = 2;
    pos posOpciones[iCantOpciones];
    posOpciones[0].y = iAltoVentana / 2;

    for (short i = 1; i < iCantOpciones; i++)
    {
        posOpciones[i].y = posOpciones[i - 1].y + iEspacioEntreOpciones;
    }
    
    int iAuxCursor = 0;

    while (!WindowShouldClose()) {

        BeginDrawing();
        ClearBackground(RAYWHITE);

        DrawText("Juego en Pausa", posTitulo.x, posTitulo.y, 50, colorTitulo);

        for (short i = 0; i < iCantOpciones; i++)
        {
            if (iAuxCursor == i)
            {
                colorOpcion = RED;
            }
            else
            {
                colorOpcion = DARKBLUE;
            }
            switch ((opcionesMenu)i)
            {
            case opcionesMenu::Continuar:
                DrawText("Continuar", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                break;            
            case opcionesMenu::Salir:
                DrawText("Salir", posOpciones[i].x, posOpciones[i].y, 30, colorOpcion);
                break;
            default:
                break;
            }

        }

        EndDrawing();

        if (IsKeyPressed(KEY_UP)) { iAuxCursor = (iAuxCursor == 0) ? iCantOpciones - 1 : iAuxCursor - 1; }
        if (IsKeyPressed(KEY_DOWN)) { iAuxCursor = (iAuxCursor == iCantOpciones - 1) ? 0 : iAuxCursor + 1; }
        if (IsKeyPressed(KEY_ENTER))
        {
            switch ((opcionesMenu)iAuxCursor)
            {
            case opcionesMenu::Continuar:
                return false;
                break;           
            case opcionesMenu::Salir:
                return true;                
                break;
            default:
                break;
            }

        }

    }

}
*/


