#ifndef VENTANA_H
#define VENTANA_H
#include "raylib.h"

namespace ventana
{
	const int iAltoVentana = 450;
	const int iAnchoVentana = 800;

	void crearVentana();
}

#endif // !VENTANA_H

